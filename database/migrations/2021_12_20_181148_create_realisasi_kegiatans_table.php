<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealisasiKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('realisasi_kegiatans')) {
            Schema::create('realisasi_kegiatans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('kegiatan_id');
                $table->string('name')->nullable();
                $table->text('value')->nullable();
                $table->string('type')->nullable();
                $table->timestamps();
    
                if (Schema::hasTable('kegiatans')) {
                    $table->foreign('kegiatan_id')->references('id')->on('kegiatans');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realisasi_kegiatans');
    }
}
