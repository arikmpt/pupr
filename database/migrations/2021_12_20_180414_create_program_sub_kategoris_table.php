<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramSubKategorisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('program_sub_kategoris')) {
            Schema::create('program_sub_kategoris', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('kode')->nullable();
                $table->string('nama')->nullable();
                $table->unsignedBigInteger('program_kategori_id');
                $table->timestamps();
    
                if (Schema::hasTable('program_kategoris')) {
                    $table->foreign('program_kategori_id')->references('id')->on('program_kategoris');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_sub_kategoris');
    }
}
