<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTahapanKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tahapan_kegiatans')) {
            Schema::create('tahapan_kegiatans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('kegiatan_id');
                $table->text('deskripsi')->nullable();
                $table->timestamps();
    
                if (Schema::hasTable('kegiatans')) {
                    $table->foreign('kegiatan_id')->references('id')->on('kegiatans');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_kegiatans');
    }
}
