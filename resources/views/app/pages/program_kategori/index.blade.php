@extends('app.layouts.master')
@section('title','Data Program Kategori')
@section('content')

<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('program.kategori.new') }}" class="btn btn-primary">Tambah Data</a>
            </div>
            <div class="card-body">
                {!! $html->table() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    {!! $html->scripts() !!}
    <script>
        $(document).ready(function() {
            $('table#dataTableBuilder tbody').on( 'click', 'td button', function (e) {
                var mode = $(this).attr("data-mode");
                var parent = $(this).parent().get( 0 );
                var parent1 = $(parent).parent().get( 0 );
                var row = $('#dataTableBuilder').DataTable().row(parent1);
                var data = row.data();
                if($(this).hasClass('btn-delete') ){
                    Swal.fire({
                        title: 'Apakah Anda Yakin Untuk Menghapus Data Ini?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yakin',
                        cancelButtonText: 'Tidak'
                    }).then((result) => {
                        if (result.value) {
                            
                            const form = {
                                id : data.id,
                            }
                            $.ajax({
                                method: 'DELETE',
                                headers: {
                                    'X-CSRF-Token': "{{ csrf_token() }}"
                                },
                                url: "{{ route('program.kategori.delete') }}",
                                dataType: 'JSON',
                                cache: false,
                                data: form,
                                success: function(result) {
                                    toastr.success(result.message, 'Success !')
                                    $('#dataTableBuilder').DataTable().ajax.reload();
                                },
                                error: function(err){
                                    toastr.error(err.responseJSON.message, 'Error !')
                                    $('#dataTableBuilder').DataTable().ajax.reload();
                                }
                            });

                        } else {
                        }
                    })
                }   
            
            })
        })
    </script>
@endpush