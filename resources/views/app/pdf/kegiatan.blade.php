<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Kegiatan</title>
</head>
<style>
    .title {
        text-align: center;
    }
    h1,
    h3,
    h4 {
        margin: 0;
    }

    .table-title,
    .table-body {
        margin-top : 25px;
        font-size: 13px;
        width: 100%;
        border: 1px solid #000;
        text-transform: uppercase;
    }

    .table-title tr td,
    .table-body tr td {
        border : 1px solid #000;
        border-spacing: 0;
    }
    
    .content-on-table {
        text-transform: uppercase;
    }

    .title-on-table {
        font-weight: bold;
    }

    .content-table {
        border-bottom: 1px solid #000;
    }

    .table-content {
        width: 100%;
        text-transform: uppercase;
    }

    .table-content tr td {
        border: 0;
    }

</style>
<body>
    <div class="title">
        <h1>LAPORAN PELAKSANAAN KEGIATAN</h1>
        <h3>{{ $data->name }}</h3>
        <h4>TRIWULAN {{ $data->tri_wulan }} - Tahun {{ $data->tahun }}</h4>
    </div>
    <table class="table-title" cellspacing="0">
        <tbody>
            <tr>
                <td style="width: 20%;">PERANGKAT DAERAH</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">DINAS PEKERJAAN UMUM DAN PENATAAN RUANG KOTA PAYAKUMBUH</td>
            </tr>
            <tr>
                <td style="width: 20%;">PROGRAM</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 10%; border-right:0;">{{ $data->program->kode }}</td>
                <td style="width: 65%; border-left:0;">{{ $data->program->nama }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 10%; border-right:0;">{{ $data->kategori->kode }}</td>
                <td style="width: 65%; border-left:0;">{{ $data->kategori->nama }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">SUB-KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 10%; border-right:0;">{{ $data->subkategori->kode }}</td>
                <td style="width: 65%; border-left:0;">{{ $data->subkategori->nama }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">PERIODE</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->kategori->kode }} {{ $data->periode }}</td>
            </tr>
        </tbody>
    </table>

    <table class="table-body" cellspacing="0">
        <tbody>
            <tr>
                <td style="width: 20%;">TUJUAN KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->tujuan }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">PENJELASAN KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->penjelasan }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">PAGU</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">
                    <div class="title-on-table">
                        RP. {{ number_format($data->pagu,0,',','.') }},-
                    </div>
                    <div class="content-on-table">
                        {{ $numberTransformer->toWords($data->pagu, 'IDR') }}
                    </div>    
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">INDIKATOR KINERJA</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->indikator_kerja }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">TAHAPAN KEGIATAN YANG DILAKSANAKAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">
                    <table cellspacing="0" class="table-content">
                        @foreach($data->tahapans as $index => $tahapan)
                            @if($index == 0)
                                <tr>
                                    <td style="padding : 5px 0px;">{{ $tahapan->deskripsi }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000;">{{ $tahapan->deskripsi }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">PERMASALAHAN KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->permasalahan }}</td>
            </tr>
            <tr>
                <td style="width: 20%;">TARGET</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">
                    <table cellspacing="0" class="table-content">
                        @foreach($data->targets as $index => $target)
                            @if($index == 0)
                                <tr>
                                    <td style="padding : 5px 0px; border-right:1px solid #000; width: 50%;">{{ $target->name }}</td>
                                    <td style="text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; border-right:1px solid #000; width: 50%;">{{ $target->name }}</td>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">REALISASI</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">
                    <table cellspacing="0" class="table-content">
                        @foreach($data->realisasi as $index => $target)
                            @if($index == 0)
                                <tr>
                                    <td style="padding : 5px 0px; border-right:1px solid #000; width: 50%;">{{ $target->name }}</td>
                                    <td style="text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; border-right:1px solid #000; width: 50%;">{{ $target->name }}</td>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">CAPAIAN KEGIATAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">
                    <table cellspacing="0" class="table-content">
                        @foreach($data->capaians as $index => $target)
                            @if($index == 0)
                                <tr>
                                    <td style="padding : 5px 0px; border-right:1px solid #000; width: 50%;"><b>{{ $target->name }}</b></td>
                                    <td style="text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; border-right:1px solid #000; width: 50%;"><b>{{ $target->name }}</b></td>
                                    <td style="padding : 5px 0px; border-top: 1px solid #000; text-align: right; width: 50%;">
                                        {{ $target->value }} {{ $target->type == "percent" ? "%" : "" }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 20%;">PENUTUPAN</td>
                <td style="width: 5%; text-align: center;">:</td>
                <td style="width: 75%;" colspan="2">{{ $data->penutup }}</td>
            </tr>
        </tbody>
    </table>

    <table style="width: 100%; margin-top: 25px;">
        <tr>
            <td style="width: 60%;">
                
            </td>
            <td style="width: 40%; text-align:center;">
                <div class="signature">
                    <p style="margin:0;">Payakumbuh, {{ $data->created_at->format('d F Y') }}</p>
                    <p style="margin:0;">Kepala Dinas Pekerjaan Umum dan Penataan Ruang</p>
                    <p style="margin-top:0; margin-bottom: 100px;">Kota Payakumbuh</p>
                    <p style="margin:0; font-weight: bold; text-decoration: underline;">MUSLIM, ST., M.SI</p>
                    <p style="margin:0;">NIP. 19750402 200003 1 805</p>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>