<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramSubKategori extends Model
{
    protected $table = 'program_sub_kategoris';

    protected $fillable = [
        "kode","nama","program_kategori_id"
    ];

    public function kategori()
    {
        return $this->belongsTo(Program::class, "program_kategori_id");
    }
}
