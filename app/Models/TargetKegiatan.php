<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TargetKegiatan extends Model
{
    protected $table = 'target_kegiatans';

    protected $fillable = [
        'kegiatan_id','name','value','type'
    ];

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'kegiatan_id');
    }
}
