<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table = 'kegiatans';

    protected $fillable = [
        'program_id','program_kategori_id','program_sub_kategori_id',
        'name','tujuan','penjelasan','pagu','indikator_kerja','permasalahan',
        'penutup','tri_wulan','tahun','periode'
    ];

    public function capaians()
    {
        return $this->hasMany(CapaianKegiatan::class, 'kegiatan_id');
    }

    public function realisasi()
    {
        return $this->hasMany(RealisasiKegiatan::class, 'kegiatan_id');
    }

    public function tahapans()
    {
        return $this->hasMany(TahapanKegiatan::class, 'kegiatan_id');
    }

    public function targets()
    {
        return $this->hasMany(TargetKegiatan::class, 'kegiatan_id');
    }

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function kategori()
    {
        return $this->belongsTo(ProgramKategori::class, 'program_kategori_id');
    }

    public function subkategori()
    {
        return $this->belongsTo(ProgramSubKategori::class, 'program_sub_kategori_id');
    }
}
