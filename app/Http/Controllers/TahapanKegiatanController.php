<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TahapanKegiatan;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;

class TahapanKegiatanController extends Controller
{
    public function index(Builder $builder, $kegiatan_id)
    {
        if (request()->ajax()) {
			return $this->datatables($kegiatan_id);
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.tahapan_kegiatan.index')->with([
            'html' => $html,
            'kegiatan_id' => $kegiatan_id
        ]);
    }

    public function datatables($kegiatan_id)
	{
        $table = new TahapanKegiatan;
        return datatables($table->where('kegiatan_id', $kegiatan_id)->get())
        ->addIndexColumn()
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='"
                    .route('kegiatan.tahapan.edit', ['kegiatan_id' => $model->kegiatan_id,'id' => $model->id])."'><i class='fas fa-edit'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'deskripsi', 'title' => 'Deskripsi'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new($kegiatan_id)
    {
        return view('app.pages.tahapan_kegiatan.new')
            ->with([
                "kegiatan_id" => $kegiatan_id
            ]);
    }

    public function edit($kegiatan_id, $id)
    {
        $data = TahapanKegiatan::findOrFail($id);

        return view('app.pages.tahapan_kegiatan.edit')
            ->with([
                "data" => $data,
                "kegiatan_id" => $kegiatan_id
            ]);
    }

    public function store(Request $request)
    {
        $store = TahapanKegiatan::create($request->all());

        return $store ? redirect()->route('kegiatan.tahapan.index', $request->kegiatan_id)->with('success', 'Data berhasil disimpan')
            : redirect()->route('kegiatan.tahapan.index',$request->kegiatan_id)->with('danger', 'Data gagal tersimpan');
    }

    public function update(Request $request)
    {
        $data = TahapanKegiatan::findOrFail($request->id);
        $data->kegiatan_id = $request->kegiatan_id;
        $data->deskripsi = $request->deskripsi;
        $store = $data->save();

        return $store ? redirect()->route('kegiatan.tahapan.index',$request->kegiatan_id)->with('success','Data berhasil disimpan')
            : redirect()->route('kegiatan.tahapan.index',$request->kegiatan_id)->with('danger','Data gagal disimpan');
    }

    public function delete(Request $request)
    {
        $data = TahapanKegiatan::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }
}
