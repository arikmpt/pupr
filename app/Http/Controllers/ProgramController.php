<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;

class ProgramController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
			return $this->datatables();
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.program.index')->with([
            'html' => $html
        ]);
    }

    public function datatables()
	{
        $table = new Program;
        return datatables($table->query())
        ->addIndexColumn()
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='".route('program.edit', $model->id)."'><i class='fas fa-edit'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'kode', 'title' => 'Kode'
            ],
            [
                'data' => 'nama', 'title' => 'Nama'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new()
    {
        return view('app.pages.program.new');
    }

    public function edit($id)
    {
        $data = Program::findOrFail($id);

        return view('app.pages.program.edit')
            ->with([
                "data" => $data
            ]);
    }

    public function store(Request $request)
    {
        $store = Program::create([
            "kode" => $request->kode,
            "nama" => $request->nama
        ]);

        return $store ? redirect()->route('program.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.index')->with('danger','Data gagal disimpan');
    }

    public function update(Request $request)
    {
        $data = Program::findOrFail($request->id);
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $store = $data->save();

        return $store ? redirect()->route('program.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.index')->with('danger','Data gagal disimpan');
    }

    public function delete(Request $request)
    {
        $data = Program::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }
}
